import { Component, EventEmitter, OnInit, Output, Input } from '@angular/core'

@Component({
  selector: 'app-filters',
  templateUrl: './filters.component.html',
  styleUrls: ['./filters.component.scss']
})
export class FiltersComponent implements OnInit {
  @Input() budgetFilterOperator
  @Input() BudgetFilterOperatorList
  @Output() filterChanged = new EventEmitter<any>()

  constructor() { }

  ngOnInit() {
  }

  filter(field, value) {
    this.filterChanged.emit({ field, value })
  }

}
