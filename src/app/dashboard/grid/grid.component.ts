import { Component, EventEmitter, OnInit, Input, Output } from '@angular/core'
import { IEmployee } from 'src/app/employees/IEmployee'

@Component({
  selector: 'app-grid',
  templateUrl: './grid.component.html',
  styleUrls: ['./grid.component.scss']
})
export class GridComponent implements OnInit {
  @Input() employees: IEmployee[]
  @Input() displayedColumns
  @Output() editClicked = new EventEmitter<any>()
  @Output() saveClicked = new EventEmitter<any>()
  @Output() cancelClicked = new EventEmitter<any>()

  constructor() { }

  ngOnInit() {
  }

  edit(record: IEmployee) {
    this.editClicked.emit(record)
  }

  save(record: IEmployee) {
    this.saveClicked.emit(record)
  }

  cancel(record: IEmployee) {
    this.cancelClicked.emit(record)
  }

}
