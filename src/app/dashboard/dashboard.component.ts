import { Component, OnInit, OnDestroy } from '@angular/core'
import { MatSnackBar } from '@angular/material'
import { Subscription } from 'rxjs'
import { EmployeesService } from 'src/app/employees/employees.service'
import { IEmployee } from 'src/app/employees/IEmployee'
import { EmployeeMetrics } from 'src/app/employees/EmployeeMetrics'
import { BudgetFilterOperator } from 'src/app/employees/BudgetFilterOperator'

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit, OnDestroy {
  editMode = false
  oldRecord: IEmployee
  employees: IEmployee[]
  employeeMetrics: EmployeeMetrics
  subscriptions: Subscription[]
  BudgetFilterOperatorList = BudgetFilterOperator
  budgetFilterOperator = BudgetFilterOperator.GreaterThan
  displayedColumns = ['actions', 'title', 'division', 'project_owner', 'budget', 'status', 'created', 'modified']

  constructor(
    private snackBar: MatSnackBar,
    private employeesSvc: EmployeesService
  ) { }



  async ngOnInit() {
    this.initFilters()

    this.subscriptions = [
      this.employeesSvc.filteredEmployees.subscribe(
        employees => this.employees = employees
      ),
      this.employeesSvc.employeeMetrics.subscribe(
        metrics => this.employeeMetrics = metrics
      )
    ]
    await this.employeesSvc.getAll()
  }

  ngOnDestroy() {
    this.subscriptions.forEach(s => s.unsubscribe())
  }

  initFilters() {
    this.employeesSvc.filters.budgetFilterOperator = this.budgetFilterOperator
  }

  edit(record: IEmployee) {
    this.editMode = true
    this.oldRecord = Object.assign({}, record)
    record.editing = true
  }

  cancel(record: IEmployee) {
    this.editMode = false
    record = Object.assign(record, this.oldRecord)
    record.editing = false
  }

  save(record: IEmployee) {
    this.editMode = false
    this.oldRecord = null
    record.editing = false

    this.employeesSvc.filter()
    this.employeesSvc.updateOverview()

    this.snackBar.open('Employee record updated', 'Close', {
      duration: 2000
    })
  }

  filter(field: string, value: any) {
    value = this.employeesSvc.formatFilterValue(field, value)
    this.employeesSvc.setFilter(field, value)
  }

}
