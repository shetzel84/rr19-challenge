import { Component, OnInit, Input } from '@angular/core';
import { EmployeeMetrics } from 'src/app/employees/EmployeeMetrics';

@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.scss']
})
export class OverviewComponent implements OnInit {
  @Input() employeeMetrics: EmployeeMetrics

  constructor() { }

  ngOnInit() {
  }

}
