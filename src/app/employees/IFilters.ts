import { BudgetFilterOperator } from './BudgetFilterOperator'

export interface IFilters {
    title?: string
    division?: string
    project_owner?: string
    status?: string
    budget?: number
    budgetFilterOperator?: BudgetFilterOperator
    createdFrom?: Date
    createdTo?: Date
    modifiedFrom?: Date
    modifiedTo?: Date
}