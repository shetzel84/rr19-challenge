import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { IEmployee } from './IEmployee'
import { BehaviorSubject } from 'rxjs'
import { EmployeeMetrics } from './EmployeeMetrics'
import { IFilters } from './IFilters'
import { BudgetFilterOperator } from './BudgetFilterOperator';

@Injectable({
  providedIn: 'root'
})
export class EmployeesService {
  employees: IEmployee[]
  filters: IFilters = { }
  filteredEmployees = new BehaviorSubject<IEmployee[]>(null)
  employeeMetrics = new BehaviorSubject<EmployeeMetrics>(new EmployeeMetrics())

  constructor(private http: HttpClient) { }

  async getAll() {
    this.employees = await this.http.get<IEmployee[]>('/assets/data.json').toPromise()

    this.filter()

    this.updateOverview()
  }

  formatFilterValue(field, value) {
    if (field === 'budget') {
      value = parseFloat(value)
    }
    return value
  }

  setFilter(field: string, value: any) {
    this.filters[field] = value

    this.filter()

    this.updateOverview()
  }

  filter() {
    const filtered = this.employees.filter((employee: IEmployee) => {
      if (this.filters.title) {
        if (!employee.title.includes(this.filters.title)) {
          return false
        }
      }

      if (this.filters.division) {
        if (this.filters.division !== employee.division) {
          return false
        }
      }

      if (this.filters.project_owner) {
        if (!employee.project_owner.includes(this.filters.project_owner)) {
          return false
        }
      }

      if (this.filters.budget) {
        const operator = this.filters.budgetFilterOperator
        if (operator === BudgetFilterOperator.EqualTo) {
          if (this.filters.budget !== employee.budget) {
            return false
          }
        } else if (operator === BudgetFilterOperator.GreaterThan) {
          if (this.filters.budget >= employee.budget) {
            return false
          }
        } else if (operator === BudgetFilterOperator.LessThan) {
          if (this.filters.budget <= employee.budget) {
            return false
          }
        }
      }

      if (this.filters.status) {
        if (this.filters.status !== employee.status) {
          return false
        }
      }

      if (this.filters.createdFrom) {
        const date = new Date(employee.created)
        if (!employee.created || this.filters.createdFrom > date) {
          return false
        }
      }

      if (this.filters.createdTo) {
        const date = new Date(employee.created)
        if (!employee.created || this.filters.createdTo < date) {
          return false
        }
      }

      if (this.filters.modifiedFrom) {
        const date = new Date(employee.modified)
        if (!employee.modified || this.filters.modifiedFrom > date) {
          return false
        }
      }

      if (this.filters.modifiedTo) {
        const date = new Date(employee.modified)
        if (!employee.modified || this.filters.modifiedTo < date) {
          return false
        }
      }

      return true
    })

    this.filteredEmployees.next(filtered)
  }

  updateOverview() {
    const employeeMetrics: EmployeeMetrics = new EmployeeMetrics()
    this.filteredEmployees.value.forEach((employee: IEmployee) => {
      const { division, budget } = employee
      employeeMetrics[division] += parseFloat(budget.toString())
    })

    this.employeeMetrics.next(employeeMetrics)
  }

}
