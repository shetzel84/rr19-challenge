export enum BudgetFilterOperator {
    GreaterThan,
    LessThan,
    EqualTo
}
