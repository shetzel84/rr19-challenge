# Improvements
1.  Validation
2.  Chips to show active filters (with delete icon)
3.  Add icons to overview boxes
4. Refactor Filter function
   1.  Move common match filters to reusable functions
5. Remove mutation of row edit
6. Do not show snack bar notification if there were no differences on save
7. Move dropdown options to data objects in typescript
8. Add unit testing around filtering and calculation functionality
9. If project is projected to grow, refactor state management from service to ngrx